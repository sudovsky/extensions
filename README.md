# Extensions

[![CI Status](https://img.shields.io/travis/Max Sudovski/Extensions.svg?style=flat)](https://travis-ci.org/Max Sudovski/Extensions)
[![Version](https://img.shields.io/cocoapods/v/Extensions.svg?style=flat)](https://cocoapods.org/pods/Extensions)
[![License](https://img.shields.io/cocoapods/l/Extensions.svg?style=flat)](https://cocoapods.org/pods/Extensions)
[![Platform](https://img.shields.io/cocoapods/p/Extensions.svg?style=flat)](https://cocoapods.org/pods/Extensions)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Extensions is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Extensions'
```

## Author

Max Sudovski, sudovsky@cloudunion.ru

## License

Extensions is available under the MIT license. See the LICENSE file for more info.
