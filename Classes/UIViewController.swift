//
//  UIViewController.swift
//  delivery
//
//  Created by Sudovsky on 01.04.2020.
//  Copyright © 2020 Sudovsky. All rights reserved.
//

import UIKit

public extension UIViewController {
    
    var screenSize: CGSize {
        var size = CGSize(width: 0, height: 0)
        
        if let window = UIApplication.shared.keyWindow {
            size = CGSize(width: window.bounds.width, height: window.bounds.height)
        } else {
            size = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }
        
        return size
    }
    

    func executeWithDelay(_ delay: TimeInterval, code: @escaping () -> Void) {
        view.executeWithDelay(delay, code: code)
    }
    
    func executeInBackground(code: @escaping () -> Void = { }, completion: @escaping () -> Void = { }) {
        
        DispatchQueue.global().async {
            let task = UIApplication.shared.beginBackgroundTask {}
            code()
            UIApplication.shared.endBackgroundTask(task)
            DispatchQueue.main.async {
                completion()
            }
        }
    }
    
    func getController(withIdentifier name: String, fromStoryboard storyboardName: String = "Main") -> UIViewController? {
        
        return UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: name)
        
    }
    
    func showControllerWithNavigation<T>(withIdentifier name: String, fromStoryboard storyboardName: String = "Main", as asType: T.Type, fillParams: @escaping (T) -> Void = { _ in }) {
        
        guard let controller = getController(withIdentifier: name, fromStoryboard: storyboardName) else { return }
        
        guard let myController = controller as? T else { return }
        
        fillParams(myController)
        
        showControllerWithNavigation(controller)
        
    }
    
    func showControllerWithNavigation(_ controller: UIViewController) {
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.navigationBar.isHidden = true
        present(navigationController, animated: true, completion: nil)
    }
    
    func showController<T>(withIdentifier name: String, fromStoryboard storyboardName: String = "Main", as asType: T.Type, fillParams: @escaping (T) -> Void = { _ in }) {
        
        guard let controller = getController(withIdentifier: name, fromStoryboard: storyboardName) else { return }
        
        guard let myController = controller as? T else { return }
        
        fillParams(myController)
        
        showController(controller)
        
    }
    
    func showController(_ controller: UIViewController) {
        present(controller, animated: true, completion: nil)
    }
    
}
