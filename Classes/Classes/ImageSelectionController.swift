//
//  ImageSelectionController.swift
//  delivery
//
//  Created by Sudovsky on 06.05.2020.
//  Copyright © 2020 Sudovsky. All rights reserved.
//

import UIKit

open class ImageDocumentSelectionController: UIViewController, UINavigationControllerDelegate, UIDocumentPickerDelegate, UIImagePickerControllerDelegate {
    
    var imagePicker = UIImagePickerController()
    private var canChange = false

    public func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        UIApplication.shared.isStatusBarHidden = true
    }
    
    open func showSelectionController(title selectionTitle: String = "Выбор изображения", canChange allowsEditing: Bool = false, showDeleteAction: Bool = true) {
        
        let alert = UIAlertController(title: selectionTitle, message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Из галереи", style: .default , handler:{ (UIAlertAction)in
            self.beginImageSelection(canChange: allowsEditing)
        }))

        alert.addAction(UIAlertAction(title: "Из файлов", style: .default , handler:{ (UIAlertAction)in
            self.beginDocumentSelection()
        }))

        if showDeleteAction {
            alert.addAction(UIAlertAction(title: "Удалить фото", style: .destructive, handler:{ (UIAlertAction)in
                self.deleteImage()
            }))
        }

        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler:{ (UIAlertAction)in
        }))

        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }

        self.present(alert, animated: true, completion: {
        })
    }

    open func beginDocumentSelection() {
        
        let documentPicker = UIDocumentPickerViewController.init(documentTypes: ["public.data"], in: .import)
        
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .fullScreen
        
        self.present(documentPicker, animated: true, completion: nil)
        
    }
    
    open func proceedSelectedDocument(_ url: URL) {
        
    }
    
    open func deleteImage() {
        
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {

        if controller.documentPickerMode == .import {
            if urls.count == 0 {
                return
            }

            proceedSelectedDocument(urls[0])
        }
        
    }
    
    open func beginImageSelection(canChange allowsEditing: Bool = false) {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            self.canChange = allowsEditing
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = allowsEditing
            imagePicker.modalPresentationStyle = .fullScreen
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    open func proceedSelectedImage(_ image: UIImage) {
        
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if canChange, let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            proceedSelectedImage(pickedImage)
        } else if !canChange, let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            proceedSelectedImage(pickedImage)
        }
        
        dismiss(animated: true)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion:nil)
    }
    
    open func resizeImage(_ image: UIImage, size: CGSize) -> UIImage {
        return image.resize(targetSize: size)
    }

}
