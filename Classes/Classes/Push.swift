//
//  Push.swift
//  iSmoked
//
//  Created by Sudovsky on 30/08/2019.
//  Copyright © 2019 Sudovsky. All rights reserved.
//

import UIKit

open class PushNotificationSender {
    
    static public func sendPushNotification(key: String, to token: String, title: String = "", body: String = "", data: [String: Any]? = nil, completion: @escaping ((Bool, String) -> Void) = {_,_ in}) {
        
        if key.trim().isEmpty {
            return
        }
        
        let urlString = "https://fcm.googleapis.com/fcm/send"
        let url = NSURL(string: urlString)!
        var paramString: [String : Any] = ["to" : token]
        
        if title.isEmpty, body.isEmpty {
            paramString["content_available"] = true
        } else {
            paramString["notification"] = ["title" : title, "body" : body, "sound" : "default"]
        }
        
        if data != nil {
            paramString["data"] =  data
        }
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=" + key, forHTTPHeaderField: "Authorization")
        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        //print("Received data:\n\(jsonDataDict))")
                        var error = ""
                        if let results = jsonDataDict["results"] as? [[String:String]] {
                            if results.count > 0 {
                                error = results.first?["error"] ?? ""
                            }
                        }
                        completion(jsonDataDict["success"] as? Bool ?? false, error)
                    }
                }
            } catch let err as NSError {
                print(err.debugDescription)
            }
        }
        task.resume()
    }
        
}
