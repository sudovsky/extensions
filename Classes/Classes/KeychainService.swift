//
//  KeychainService.swift
//  docmgr
//
//  Created by Sudovsky on 27/09/2019.
//  Copyright © 2019 Max Sudovsky. All rights reserved.
//

import Foundation

public class KeychainService {
    
    public static func save(_ password: String, for account: String) {
        
        if retrivePassword(for: account) != nil {
            removePassword(for: account)
        }
        
        let password = password.data(using: String.Encoding.utf8)!
        let query: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrAccount as String: account,
                                    kSecValueData as String: password]
        let status = SecItemAdd(query as CFDictionary, nil)
        guard status == errSecSuccess else {
            return print("save error")
        }
    }
 
    public static func retrivePassword(for account: String) -> String? {
        let query: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrAccount as String: account,
                                    kSecMatchLimit as String: kSecMatchLimitOne,
                                    kSecReturnData as String: kCFBooleanTrue]
        
        var retrivedData: AnyObject? = nil
        let _ = SecItemCopyMatching(query as CFDictionary, &retrivedData)
        
        guard let data = retrivedData as? Data else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    public static func removePassword(for account: String) {
        let query: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrAccount as String: account]
        
        let status = SecItemDelete(query as CFDictionary)
        guard status == errSecSuccess else {
            return print("Delete error")
        }
    }
    
    public static func removeAllPasswords() {
        let secItemClasses =  [kSecClassGenericPassword]
        for itemClass in secItemClasses {
            let spec: NSDictionary = [kSecClass: itemClass]
            SecItemDelete(spec)
        }
    }
    
}
