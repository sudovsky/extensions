//
//  SwitchView.swift
//  docmgr
//
//  Created by Sudovsky on 07/10/2019.
//  Copyright © 2019 Max Sudovsky. All rights reserved.
//

import UIKit

public protocol SwitchViewDelegate {
    func switchingWillBegin(from: Int, to: Int)
    func switchingDidEnd(from: Int, to: Int)
}

public extension SwitchViewDelegate {
    func switchingWillBegin(from: Int, to: Int) { }
    func switchingDidEnd(from: Int, to: Int) { }
}

public struct SwitchItem {
    public var title = ""
    public var action: () -> Void = { }
    public var font: UIFont? = nil
    
    public init(title: String, font: UIFont? = nil, action: @escaping () -> Void = { }) {
        self.title = title
        self.action = action
        self.font = font
    }
}

open class SwitchView: UIView {
    
    // MARK: - Variables
    
    @IBInspectable open var backColor: UIColor = UIColor.black.withAlphaComponent(0.1) {
        didSet {
            backgroundColor = backColor
            highlightView.layer.borderColor = backgroundColor?.cgColor
        }
    }
    
    @IBInspectable open var highlightColor: UIColor = .white {
        didSet {
            highlightView.backgroundColor = highlightColor
        }
    }
    
    @IBInspectable open var titleColor: UIColor = .black {
        didSet {
            updateColors()
        }
    }
    
    @IBInspectable open var selectedTitleColor: UIColor = .black {
        didSet {
            updateColors()
        }
    }
    
    open var delegate: SwitchViewDelegate? = nil
    
    open var highlightView = UIView()
    open var selectedItem = 0
    private var highlightPadding: CGFloat = 1
    
    open var items = [SwitchItem]()
    open var buttons = [UIButton]()
    open var inactiveButtonAplha: CGFloat = 0.4
    
    private var customBackground = false
    
    open override var backgroundColor: UIColor? {
        get {
            return super.backgroundColor
        }
        
        set {
            super.backgroundColor = newValue
            customBackground = true
            highlightView.layer.borderColor = newValue?.cgColor
        }
    }
    
    // MARK: - Initialization
    
    open func updateColors() {
        
        backgroundColor = backColor
        highlightView.layer.borderColor = backgroundColor?.cgColor

        highlightView.backgroundColor = highlightColor
        
        for btn in buttons {
            if btn.tag == selectedItem {
                btn.setTitleColor(selectedTitleColor, for: .normal)
            } else {
                btn.setTitleColor(titleColor, for: .normal)
            }
        }
    }
    
    private func configureUI() {
        backgroundColor = backColor
        layer.cornerRadius = 10

        let btnWidth: CGFloat = frame.width / CGFloat(items.count)
        var currentLeft: CGFloat = 0
        
        highlightView = UIView(frame: CGRect(x: 0,
                                             y: 0,
                                             width: btnWidth,
                                             height: frame.height))
        highlightView.layer.borderColor = backgroundColor?.cgColor
        highlightView.layer.borderWidth = highlightPadding
        highlightView.backgroundColor = .white//.cellBackground
        highlightView.layer.cornerRadius = layer.cornerRadius
        
        addSubview(highlightView)
        
        var t = 0
        
        for btn in buttons {
            btn.removeFromSuperview()
        }
        buttons = []
        
        for item in items {
            let btn = createButton(frame: CGRect(x: currentLeft, y: 0, width: btnWidth, height: frame.height), title: item.title, font: item.font)
            if t == 0 {
                btn.alpha = 1
            }
            btn.tag = t
            
            t += 1
            
            buttons.append(btn)
            
            currentLeft += btnWidth
        }
        
        //let panGR = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        //panGR.delegate = self as? UIGestureRecognizerDelegate

        //self.addGestureRecognizer(panGR)

    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    convenience public init(frame: CGRect, items itms: [SwitchItem]) {
        self.init(frame: frame)
        items = itms
        configureUI()
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func createButton(frame: CGRect, title: String, font: UIFont? = nil) -> UIButton {
        let btn = UIButton(frame: frame)
        btn.setTitle(title, for: .normal)
        btn.titleLabel?.font = font ?? UIFont.systemFont(ofSize: 20, weight: .medium)
        btn.setTitleColor(titleColor, for: .normal)
        btn.addTarget(self, action: #selector(changeSelection), for: .touchUpInside)
        btn.titleLabel?.adjustsFontSizeToFitWidth = true
        btn.titleLabel?.numberOfLines = 2
        btn.titleLabel?.lineBreakMode = .byWordWrapping
        btn.titleLabel?.minimumScaleFactor = 0.5
        btn.titleLabel?.textAlignment = .center
        btn.titleEdgeInsets = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        btn.alpha = inactiveButtonAplha
        addSubview(btn)
        return btn
    }
    
    // MARK: - Actions
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        let btnWidth: CGFloat = frame.width / CGFloat(items.count)
        var currentLeft: CGFloat = 0
        
        if highlightView.width != btnWidth {
            highlightView.frame = CGRect(x: btnWidth * CGFloat(selectedItem),
                                         y: 0,
                                         width: btnWidth,
                                         height: frame.height)
        }

        for btn in buttons {
            btn.frame = CGRect(x: currentLeft, y: 0, width: btnWidth, height: frame.height)
            currentLeft += btnWidth
        }
        
        //setSelectedItem(selectedItem)

    }
    
    open func addItems(_ newItems: [SwitchItem]) {
        items += newItems
        let btnWidth: CGFloat = frame.width / CGFloat(items.count)
        var currentLeft: CGFloat = 0
        var t = buttons.count
        
        for btn in buttons {
            btn.frame = CGRect(x: currentLeft, y: 0, width: btnWidth, height: frame.height)
            currentLeft += btnWidth
        }
        
        for item in newItems {
            let btn = createButton(frame: CGRect(x: currentLeft, y: 0, width: btnWidth, height: frame.height), title: item.title, font: item.font)
            btn.tag = t
            buttons.append(btn)

            t += 1
            currentLeft += btnWidth
        }
        
        setSelectedItem(selectedItem)
        
    }
    
    open func setSelectedItem(_ index: Int, animated: Bool = false, completion: @escaping () -> Void = { }) {
        
        if index >= items.count {
            return
        }
        
        if selectedItem == index, animated {
            completion()
            return
        }
        
        let forward = selectedItem < index
        
        let btnWidth: CGFloat = frame.width / CGFloat(items.count)
        let btnFrame = buttons[index].frame
        
        delegate?.switchingWillBegin(from: selectedItem, to: index)
        
        if animated {
            
            let dur: Double = 0.15
            
            UIView.animate(withDuration: dur, delay: 0, options: .curveEaseIn, animations: {
                
                if forward {
                    self.highlightView.frame = CGRect(x: self.highlightView.frame.minX,
                                                      y: 0,
                                                      width: btnWidth * CGFloat(index - self.selectedItem + 1),
                                                      height: btnFrame.height)
                } else {
                    self.highlightView.frame = CGRect(x: btnFrame.minX,
                                                      y: 0,
                                                      width: btnWidth * CGFloat(self.selectedItem - index + 1),
                                                      height: btnFrame.height)
                }
                
                self.buttons[self.selectedItem].alpha = self.inactiveButtonAplha
                self.buttons[index].alpha = 1

            })
            
            UIView.animate(withDuration: dur, delay: dur, options: .curveEaseOut, animations: {
                self.highlightView.frame = btnFrame
            }) { (done) in
                self.delegate?.switchingDidEnd(from: self.selectedItem, to: index)
                self.selectedItem = index
                completion()
            }
            
            UIView.transition(with: self.buttons[index], duration: dur, options: .transitionCrossDissolve, animations: {
                self.buttons[index].setTitleColor(self.selectedTitleColor, for: .normal)
            }, completion: nil)
            
            UIView.transition(with: self.buttons[self.selectedItem], duration: dur, options: .transitionCrossDissolve, animations: {
                self.buttons[self.selectedItem].setTitleColor(self.titleColor, for: .normal)
            }, completion: nil)
            
        } else {
            highlightView.frame = btnFrame
            buttons[selectedItem].alpha = inactiveButtonAplha
            buttons[index].setTitleColor(selectedTitleColor, for: .normal)
            buttons[self.selectedItem].setTitleColor(titleColor, for: .normal)
            buttons[index].alpha = 1
            delegate?.switchingDidEnd(from: selectedItem, to: index)
            selectedItem = index
            completion()
        }
        
    }
    
    @objc open func handlePan(_ gesture: AnyObject){
        
        if gesture.state == UIGestureRecognizer.State.ended {
            
            let point = gesture.velocity(in: self)
            if abs(point.x) <= abs(point.y) {
                return
            }

            var newItem = selectedItem
            if point.x > 10 {
                newItem += 1
            } else if point.x < -10 {
                newItem -= 1
            }
            
            if newItem < 0 || newItem >= items.count {
                return
            }
            
            setSelectedItem(newItem, animated: true) {
                self.items[newItem].action()
            }

        }
        
    }

    @objc open func changeSelection(sender: Any) {
        let btn = sender as? UIButton
        let tag = btn?.tag ?? 0
        
        setSelectedItem(tag, animated: true) {
            self.items[tag].action()
        }
    }
    
}
