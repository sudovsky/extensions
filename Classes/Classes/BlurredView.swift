//
//  BlurredView.swift
//  docmgr
//
//  Created by Sudovsky on 09.10.2019.
//  Copyright © 2019 Max Sudovsky. All rights reserved.
//

import UIKit

open class BluriedView: UIView {
    
    @IBInspectable open var darkStyle: Bool = false {
        didSet {
            style = darkStyle ? .dark : .light
        }
    }

    open var style: UIBlurEffect.Style = .light {
        didSet {
            blurEffect = UIBlurEffect(style: style)
            visualEffectView.effect = blurEffect
        }
    }

    var blurEffect = UIBlurEffect()
    open var visualEffectView = UIVisualEffectView()
    open var blurEnabled: Bool = true {
        didSet {
            visualEffectView.isHidden = !blurEnabled
        }
    }
    
    private func create(effect: UIBlurEffect.Style? = nil) {
        
        clipsToBounds = true
        
        backgroundColor = .clear
        
        blurEffect = UIBlurEffect(style: effect ?? style)
        
        visualEffectView = UIVisualEffectView(effect: blurEffect)
        visualEffectView.frame = bounds
        visualEffectView.clipsToBounds = true
        
        insertSubview(visualEffectView, at: 0, constraints: [.leading, .top, .trailing, .bottom])

    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        create()
    }
    
    convenience public init(frame: CGRect, effectStyle: UIBlurEffect.Style) {
        self.init(frame: frame)
        
        create(effect: effectStyle)
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        create()
    }

    open func refresh(effectStyle: UIBlurEffect.Style? = nil) {
        
        visualEffectView.removeFromSuperview()
        blurEffect = UIBlurEffect(style: effectStyle ?? style)
        visualEffectView = UIVisualEffectView(effect: blurEffect)
        visualEffectView.frame = bounds
        visualEffectView.clipsToBounds = true
        
        insertSubview(visualEffectView, at: 0, constraints: .standartConstraints)

    }
    
}
