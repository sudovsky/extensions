//
//  UIButton.swift
//  Extensions
//
//  Created by Sudovsky on 15.05.2020.
//

import UIKit

public extension UIButton {
    
    func setImageColor(color: UIColor) {
        
        guard let templateImage = self.image(for: .normal)?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate) else { return }
        self.setImage(templateImage, for: .normal)
        self.tintColor = color

    }
    
}
