//
//  UIView.swift
//  Pods
//
//  Created by Sudovsky on 15.05.2020.
//

import UIKit

public extension UIView {

    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    var top: CGFloat {
        get { return frame.minY }
        set { frame = CGRect(x: frame.minX, y: newValue, width: frame.width, height: frame.height) }
    }
    
    var left: CGFloat {
        get { return frame.minX }
        set { frame = CGRect(x: newValue, y: frame.minY, width: frame.width, height: frame.height) }
    }
    
    var height: CGFloat {
        get { return frame.height }
        set { frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: newValue) }
    }
    
    var width: CGFloat {
        get { return frame.width }
        set { frame = CGRect(x: frame.minX, y: frame.minY, width: newValue, height: frame.height) }
    }
    
    var bottom: CGFloat {
        get { top + height }
    }
    
    var bottomArea: CGFloat {
        
        if #available(iOS 11.0, *) {
            let tempWindow = UIWindow()
            return tempWindow.safeAreaInsets.bottom
        } else {
            return 0
        }
        
    }

    var topArea: CGFloat {
        
        if #available(iOS 12.0, *) {
            let tempWindow = UIWindow()
            return tempWindow.safeAreaInsets.top
        } else if #available(iOS 11.0, *) {
            let tempWindow = UIWindow()
            return tempWindow.safeAreaInsets.top == 0 ? 20 : tempWindow.safeAreaInsets.top
        } else {
            return 20
        }
        
    }

    func toImage() -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, true, 0)
        guard let context = UIGraphicsGetCurrentContext() else { return UIImage() }
        layer.render(in: context)
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return UIImage() }
        UIGraphicsEndImageContext()
        
        return image

    }
    
    // OUTPUT 1
    func dropShadow() {
    
        self.dropShadow(color: .black, opacity: 0.1, offSet: CGSize(width: 0, height: 2), radius: 3, scale: true)

    }
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func addCornerRadiusAnimation(from: CGFloat, to: CGFloat, duration: CFTimeInterval)
    {
        let animation = CABasicAnimation(keyPath:"cornerRadius")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.fromValue = from
        animation.toValue = to
        animation.duration = duration
        layer.add(animation, forKey: "cornerRadius")
        layer.cornerRadius = to
    }

    func applyConstraints(_ constraints: [NSLayoutConstraint.Attribute], for view: UIView) {
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        for attr in constraints {
            
            switch attr {
            case .bottom:
                let const = NSLayoutConstraint(item: view, attribute: attr, relatedBy: .equal, toItem: self, attribute: attr, multiplier: 1, constant: -(bounds.height - view.frame.maxY))
                if let identifier = view.accessibilityIdentifier, identifier != "" {
                    const.identifier = identifier + ".bottom"
                }
                const.isActive = true
            case.top:
                let const = NSLayoutConstraint(item: view, attribute: attr, relatedBy: .equal, toItem: self, attribute: attr, multiplier: 1, constant: view.frame.minY)
                if let identifier = view.accessibilityIdentifier, identifier != "" {
                    const.identifier = identifier + ".top"
                }
                const.isActive = true
            case.leading:
                let const = NSLayoutConstraint(item: view, attribute: attr, relatedBy: .equal, toItem: self, attribute: attr, multiplier: 1, constant: view.frame.minX)
                if let identifier = view.accessibilityIdentifier, identifier != "" {
                    const.identifier = identifier + ".leading"
                }
                const.isActive = true
            case.trailing:
                let const = NSLayoutConstraint(item: view, attribute: attr, relatedBy: .equal, toItem: self, attribute: attr, multiplier: 1, constant: -(bounds.width - view.frame.maxX))
                if let identifier = view.accessibilityIdentifier, identifier != "" {
                    const.identifier = identifier + ".trailing"
                }
                const.isActive = true
            case .height:
                let const = NSLayoutConstraint(item: view, attribute: attr, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.height)
                if let identifier = view.accessibilityIdentifier, identifier != "" {
                    const.identifier = identifier + ".height"
                }
                const.isActive = true
            case .width:
                let const = NSLayoutConstraint(item: view, attribute: attr, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.width)
                if let identifier = view.accessibilityIdentifier, identifier != "" {
                    const.identifier = identifier + ".width"
                }
                const.isActive = true
            case .centerX:
                let const = NSLayoutConstraint(item: view, attribute: attr, relatedBy: .equal, toItem: self, attribute: attr, multiplier: 1, constant: 0)
                if let identifier = view.accessibilityIdentifier, identifier != "" {
                    const.identifier = identifier + ".centerX"
                }
                const.isActive = true
            case .centerY:
                let const = NSLayoutConstraint(item: view, attribute: attr, relatedBy: .equal, toItem: self, attribute: attr, multiplier: 1, constant: 0)
                if let identifier = view.accessibilityIdentifier, identifier != "" {
                    const.identifier = identifier + ".centerY"
                }
                const.isActive = true
            default:
                return
            }
            
        }
        
    }
    
    func addSubview(_ view: UIView, constraints: [NSLayoutConstraint.Attribute]) {
        addSubview(view)
        applyConstraints(constraints, for: view)
    }
    
    func insertSubview(_ view: UIView, belowSubview: UIView, constraints: [NSLayoutConstraint.Attribute]){
        insertSubview(view, belowSubview: belowSubview)
        applyConstraints(constraints, for: view)
    }
    
    func insertSubview(_ view: UIView, aboveSubview: UIView, constraints: [NSLayoutConstraint.Attribute]){
        insertSubview(view, aboveSubview: aboveSubview)
        applyConstraints(constraints, for: view)
    }
    
    func insertSubview(_ view: UIView, at: Int, constraints: [NSLayoutConstraint.Attribute]){
        insertSubview(view, at: at)
        applyConstraints(constraints, for: view)
    }
    
    func roundCorners(_ corners: CACornerMask, radius: CGFloat = 20) -> Bool {
        if #available(iOS 11.0, *) {
            layer.cornerRadius = radius
            layer.maskedCorners = corners
            return true
        }
        
        return false
    }
    
    func windowSize() -> CGSize {
        
        var size = CGSize(width: 0, height: 0)
        
        if let window = UIApplication.shared.keyWindow {
            size = CGSize(width: window.bounds.width, height: window.bounds.height)
        } else {
            size = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }
        
        return size
        
    }
    
    func hideSlowly(wait needToWait: Bool = true, completion: @escaping () -> Void = { }) {
        
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0
        }) { (done) in
            if needToWait {
                completion()
            }
        }
        
        if !needToWait{
            completion()
        }
        
    }
    
    func showSlowly(wait needToWait: Bool = true, completion: @escaping () -> Void = { }) {
        
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1
        }) { (done) in
            if needToWait{
                completion()
            }
        }
        
        if !needToWait {
            completion()
        }
        
    }
    
    func executeWithDelay(_ delay: TimeInterval, code: @escaping () -> Void) {
        _ = Timer.scheduledTimer(withTimeInterval: delay, repeats: false, block: { (timer) in
            code()
        })
    }
    
    func copyView<T: UIView>() -> T {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as! T
    }
    
}


