//
//  CGRect.swift
//  ALR
//
//  Created by Sudovsky on 05/09/2019.
//  Copyright © 2019 Sudovsky. All rights reserved.
//

import UIKit

public extension CGRect {
    
    func rectInCenter(newWidth: CGFloat = 0, newHeight: CGFloat = 0, upPadding: CGFloat = 0) -> CGRect {
        let hh = newHeight == 0 ? newWidth : newHeight
        let up = upPadding == 0 ? (height / 2 - hh / 2) + minY : upPadding
        let rect = CGRect(x: (width / 2 - newWidth / 2) + minX,
                          y: up,
                          width: newWidth,
                          height: hh)
        return rect
    }
    
}
