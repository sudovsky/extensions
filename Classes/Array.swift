//
//  Array.swift
//  Extensions
//
//  Created by Sudovsky on 15.05.2020.
//

import UIKit

public extension Array where Element == NSLayoutConstraint.Attribute {

    static var standartConstraints = [NSLayoutConstraint.Attribute.leading, NSLayoutConstraint.Attribute.trailing, NSLayoutConstraint.Attribute.bottom, NSLayoutConstraint.Attribute.top]
    
}

