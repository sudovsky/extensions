//
//  UIApplication.swift
//  docmgr
//
//  Created by Sudovsky on 23.10.2019.
//  Copyright © 2019 Max Sudovsky. All rights reserved.
//

import UIKit
import UserNotifications

public extension UIApplication {

    static var energyEconomyMode: Bool {
        get {
            if #available(iOS 9.0, *) {
                return ProcessInfo.processInfo.isLowPowerModeEnabled
            } else {
                return false
            }
        }
    }

    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {

        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)

        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)

        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
    
    func setRootController<T>(named: String, in storyboard: String = "Main", as asType: T.Type, fillParams: @escaping (T) -> Void = { _  in }) {
        
        let story = UIStoryboard.init(name: storyboard, bundle: nil)
        guard let nextController = story.instantiateViewController(withIdentifier: named) as? T else { return }
        fillParams(nextController)
        if let window = UIApplication.shared.keyWindow {
            if window.rootViewController != nil, window.rootViewController != UIApplication.getTopViewController() {
                window.rootViewController!.dismiss(animated: true) {
                    window.rootViewController = nextController as? UIViewController
                    window.makeKeyAndVisible()
                }
            } else {
                window.rootViewController = nextController as? UIViewController
                window.makeKeyAndVisible()
            }
        }

    }
    
    func setRootControllerWithNavigation<T>(named: String, in storyboard: String = "Main", as asType: T.Type, fillParams: @escaping (T) -> Void = { _  in }) {
        
        let story = UIStoryboard.init(name: storyboard, bundle: nil)
        guard let nextController = story.instantiateViewController(withIdentifier: named) as? T else { return }
        fillParams(nextController)
        if let window = UIApplication.shared.keyWindow {
            if window.rootViewController != nil, window.rootViewController != UIApplication.getTopViewController() {
                window.rootViewController!.dismiss(animated: true) {
                    let nnavigationController = UINavigationController(rootViewController: nextController as! UIViewController)
                    
                    nnavigationController.modalPresentationStyle = .fullScreen
                    nnavigationController.navigationBar.isHidden = true

                    window.rootViewController = nnavigationController
                    window.makeKeyAndVisible()
                }
            } else {
                let nnavigationController = UINavigationController(rootViewController: nextController as! UIViewController)

                nnavigationController.modalPresentationStyle = .fullScreen
                nnavigationController.navigationBar.isHidden = true

                window.rootViewController = nnavigationController
                window.makeKeyAndVisible()
            }
        }

    }
    
}
    
